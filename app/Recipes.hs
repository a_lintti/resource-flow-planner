{-# LANGUAGE OverloadedStrings #-}

module Recipes where

import Graph

import System.IO
import Data.Aeson
import Data.Aeson.Types
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.HashMap.Strict as HM
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as BL

data RecipeListJSON = RecipeListJSON {
    recipeList :: [RecipeJSON]
}
    deriving (Show)

data RecipeJSON = RecipeJSON {
    _name :: String,
    _category :: Category,
    _inputs :: [ResourceAmount],
    _outputs :: [ResourceAmount]
}
    deriving (Show)

data ResourceAmount = ResourceAmount {
    res :: String,
    amount :: Double
}
    deriving (Show)

data Category = Category {
    catName :: String
}
    deriving (Show)

instance FromJSON RecipeListJSON where
    parseJSON (Object o) = 
        return . 
        RecipeListJSON . 
        foldr (\(_, fields) y -> case parseMaybe parseJSON fields :: Maybe RecipeJSON of
            Just r -> r:y
            Nothing -> y
        ) []
        $ HM.toList o 
    parseJSON _ = fail "Top level recipes fail"

instance FromJSON RecipeJSON where
    parseJSON (Object o) =
        RecipeJSON  <$> o .: "name"
                    <*> o .: "group"
                    <*> o .: "ingredients"
                    <*> o .: "products"
    parseJSON _ = fail "parsing Recipe failed"

instance FromJSON ResourceAmount where
    parseJSON (Object o) =
        ResourceAmount  <$> o .: "name"
                        <*> o .: "amount"
    parseJSON _ = fail "parsing Resource failed"

instance FromJSON Category where
    parseJSON (Object o) =
        Category  <$> o .: "name"
    parseJSON _ = fail "parsing Category failed"


recipeJSONToRecipe :: RecipeJSON -> (String, Recipe)
recipeJSONToRecipe (RecipeJSON n c i o) = (catName c, Recipe n (convert i) (convert o))
    where
        convert l = foldr (\x y -> (res x, amount x):y) [] l

getGategories :: FilePath -> IO (Maybe [String])
getGategories f = do
    recipes <- getRecipes f
    case recipes of
        Nothing -> return Nothing
        Just ls -> return . Just $ uniqueGategories (fst $ unzip ls)
    where
        uniqueGategories :: [String] -> [String]
        uniqueGategories rs = 
            Set.elems . Set.fromList $ foldr (\r p -> r:p) [] rs

getRecipes :: FilePath -> IO (Maybe [(String, Recipe)])
getRecipes f = do
    contents <- readFile f
    return $ fmap convert (decode (BL.pack contents) :: Maybe RecipeListJSON )
    where 
        convert :: RecipeListJSON -> [(String, Recipe)]
        convert (RecipeListJSON xs) = foldr (\x y -> (recipeJSONToRecipe x):y) [] xs

getResources :: FilePath -> IO (Maybe [Resource])
getResources f = do
    recipes <- getRecipes f
    case recipes of
        Nothing -> return Nothing
        Just ls -> return . Just . Set.elems $ uniqueResources (snd $ unzip ls)
    where
        uniqueResources rs = 
            Set.fromList . fst . unzip $ foldr (\r p -> inputs r++outputs r++p) [] rs



