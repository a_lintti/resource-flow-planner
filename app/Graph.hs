module Graph where

import Data.IORef
import qualified Data.Text as T
import Data.Array
import qualified Data.Map as Map

-- The Factory is a directed graph where Resources flow.
-- We define a number of Sinks that each want a set amount of 
-- specified Resource. Rest of the factory is resolved such that 
-- the Sinks are filled.
-- Machines convert/generate Resources according to Recipes.
data Node = Machine Recipe Double
          | Sink Resource Double (IORef T.Text)
          | EmptyNode
    deriving (Eq)--, Show)

-- Recipe has input and output resources and ratios
data Recipe = Recipe {
                    name :: String,
                    inputs :: [(Resource, Double)], 
                    outputs :: [(Resource, Double)]}
    deriving (Eq, Show)

-- Resource is fancy String
type Resource = String

-- Graph Definitions
type Vertex = Int
type Table a = Array Vertex a
type Graph e = Table [(e, Vertex)] -- Neighbour lists
type Edge e = (Vertex, e, Vertex)


-- Given node and its outputs, returns node with correct scale factor
setNodeQuantity :: Node -> [(Resource, Double)] -> Node
setNodeQuantity (Sink res q r) _ = Sink res q r
setNodeQuantity (Machine rec q) outRes = Machine rec maxQ
    where
        maxQ = foldr (\x y -> max (reqQ x) y) q (zip (outputs rec) outRes)
        reqQ ((recR, recQ), (outR, outQ)) = 
            if recR==outR
            then outQ/recQ
            else 0

-- Given a vertex, sets weights for the edges that lead to that vertex.
-- The edge values are calculated according to the node in the vertex
setEdgeQuantities :: Vertex 
                  -> Graph (Resource, Double)
                  -> Table Node 
                  -> Graph (Resource, Double)
setEdgeQuantities vertex graph nodes = 
    graph // (update (getInputVerticies vertex graph))
    where
        -- Take list of vertices and update neighbour lists for them.
        -- The edge quantity being the changing property
        update :: [Vertex] -> [(Vertex, [((Resource, Double), Vertex)])]
        update [] = []
        update (v:vs) = (v, updateNeighbours (graph ! v)):(update vs)
        -- Take neighbour list (+ node info from one level up) 
        -- and update the quantity of the correct neighbour
        updateNeighbours :: [((Resource, Double), Vertex)] 
                         -> [((Resource, Double), Vertex)]
        updateNeighbours [] = []
        updateNeighbours (((resa, qa), v):es) = case nodes ! vertex of
            -- Neighbours vertex (v) must match 
            -- the vertex we are in (vertex)
            -- If the node is machine, get the recipe quantity
            Machine rec qb  -> 
                case getRecipeQuantity resa (inputs rec) of
                    -- Nothing means that there is no matching resource
                    -- -> do not change anything
                    Nothing -> ((resa, qa), v):(updateNeighbours es)
                    Just q  -> 
                        if v==vertex 
                        -- recipe quantity * machine quantity 
                        -- = required edge quantity
                        then ((resa, qb*q), v):(updateNeighbours es)
                        else ((resa, qa), v):(updateNeighbours es)
            -- If the node is Sink and other properties match
            -- change the quantity
            Sink resb qb _  -> 
                if resa==resb && v==vertex 
                then ((resa, qb), vertex):(updateNeighbours es)
                else ((resa, qa), v):(updateNeighbours es)

getRecipeQuantity :: Resource -> [(Resource, Double)] -> Maybe Double
getRecipeQuantity _ [] = Nothing
getRecipeQuantity res ((r, q):rec)
    | r==res = Just q
    | otherwise = getRecipeQuantity res rec

getSinks :: Table Node -> [Vertex]
getSinks nodes = 
    foldr (\(v, n) y -> if isSink n then v:y else y) [] (assocs nodes)
    where 
        isSink (Sink _ _ _) = True
        isSink _ = False

getOutputs :: Vertex -> Graph (Resource, Double) -> [(Resource, Double)]
getOutputs v graph = Map.toList (foldr go Map.empty (graph ! v))
    where
        go ((res, q), _) m = Map.alter (\x -> update x q) res m
        update (Just x) y = Just (x+y)
        update Nothing y = Just y

getInputVerticies :: Vertex -> Graph a -> [Vertex]
getInputVerticies v graph =
    foldr (\(x, xs) -> if isInput xs then (x:) else id) [] (assocs graph)
        where
            isInput [] = False
            isInput ((_, vx):as) 
                | v==vx = True
                | otherwise = isInput as

-- The factory is resolved starting from the sinks.
-- Each sink is used as a starting point once.
resolveGraph :: (Graph (Resource, Double), Table Node)
             -> (Graph (Resource, Double), Table Node)
resolveGraph (graph, nodes) = 
    foldr (\x y -> go x y []) (graph, nodes) (getSinks nodes)
        where
            go v (g, n) visited
                | elem v visited = (g, n)
                | otherwise = 
                    foldr 
                        (\x y -> go x y (v:visited))
                        (updatedGraph v g (updatedNodes v g n), updatedNodes v g n)
                        (getInputVerticies v g)
            updatedNodes v g n = 
                (n // [(v, setNodeQuantity (n ! v) (getOutputs v g))])
            updatedGraph v g n = setEdgeQuantities v g n

graphFromEdges :: [Edge a] -> Graph a
graphFromEdges es = 
    accumArray (\x y -> y:x) [] (minVertex, maxVertex) [(a,(b,c)) | (a,b,c) <- es]
    where
        minVertex = foldr (\x -> min (edgeStart x)) (edgeStart . head $ es) es
        maxVertex = foldr (\x -> max (edgeStart x)) (edgeStart . head $ es) es
        edgeStart (s, _, _) = s

