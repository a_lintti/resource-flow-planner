{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
module GUI where

import Graph
import qualified Recipes as Recipes

import qualified Data.Text as T
import Text.Read (readMaybe)
import Data.Array
import qualified Data.Map as Map
import qualified Data.Vector.Storable as SV

import Control.Monad
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad (unless, when)

import Foreign.C.Types (CInt(..))
import Foreign.Ptr
import Foreign
import Foreign.C.String

import Data.IORef
import Data.Word

import DearImGui
import DearImGui.SDL
import DearImGui.SDL.OpenGL
import DearImGui.OpenGL3
import qualified DearImGui.Raw as Raw
import qualified DearImGui.Raw.DrawList as DrawList
import Graphics.GL
import qualified SDL

import Linear (V4(..), V2(..))


data AppConfig = AppConfig {
    window :: SDL.Window,
    nodeForms :: [IORef ImVec2],
    tabBools :: [IORef Bool],
    recipes :: [(String, Recipe)],
    resources :: [Resource],
    categories :: [String]
}

data Action = EdgeConnectIn Vertex Resource 
            | EdgeConnectOut Vertex Resource 
            | NonAction
    deriving (Show, Eq)

data AppState = AppState {
    edges :: [Edge (Resource, Double)],
    nodes :: Table Node,
    action :: Action,
    change :: Bool
}


edgeStartOffsetX = 15
edgeEndOffsetX = -15
edgeInitialStraight = 40

sinkWidth = 170
sinkHeight :: Int
sinkHeight = 65

nodeWindowFlags = 51
menuWindowFlags = 0
tabBarFlags = 1

nodeWidth = 100
nodeBaseHeight = 60
nodeExtraHeight = 25

nodeResourceHeight = 30
nodeResourceWidth = 80

nodeInResourceOffset = -30
nodeOutResourceOffset = -50



setAction :: Action -> AppState -> AppState
setAction a (AppState e n _ l) = AppState e n a l

drawNode :: IO (AppState) -> [IORef ImVec2] -> Vertex -> Node -> IO (AppState)
drawNode state nodeForm v EmptyNode = state
drawNode state nodeForm v (Sink res a ref) = do
    setNextWindowSize (head nodeForm) ImGuiCond_None
    withCString (res++" sink###"++(show v)) \namePtr ->
        with (0) \boolPtr ->
            Raw.begin 
                namePtr 
                (Just boolPtr) 
                (Just $ ImGuiWindowFlags nodeWindowFlags)
    
    let tableO = TableOptions ImGuiTableFlags_None (ImVec2 (sinkWidth) (realToFrac nodeExtraHeight)) 0
    
    beginTable tableO (T.pack $ show v) 2 
    
    Raw.tableNextRow ImGuiTableRowFlags_None (nodeExtraHeight-2)
    Raw.tableNextColumn 
    inA <- do
        button (T.pack res) 
            >>= \case
                False -> return NonAction
                True -> return (EdgeConnectIn v res)
    
    Raw.tableNextColumn
    remBut <- button "Remove"
    newState <- case (remBut, inA) of
        (True, _)  -> do
            st <- state
            return (AppState 
                (removeEdges v (edges st)) 
                (removeNode v (nodes st)) 
                (action st)
                (change st))
        (_, EdgeConnectIn v1 res1) -> do
            st <- state
            case action st of
                EdgeConnectOut v2 res2  -> 
                    if res2==res1
                    then return (AppState
                        (addEdge (v2, (res1, 0.0), v1) (edges st))
                        (nodes st)
                        NonAction
                        True)
                    else state
                EdgeConnectIn v2 res2   -> 
                    if v2==v1 && res1 == res2
                    then return $ setAction NonAction st
                    else return $ setAction (EdgeConnectOut v1 res1) st
                _                       -> 
                    return $ setAction (EdgeConnectIn v1 res1) st
        _ -> state
    
    endTable
    
    inputText (T.pack $ ("###"++show v)) ref 100
    nas <- readIORef ref
    end
    case (readMaybe (T.unpack nas) :: Maybe Double) of 
        Just na  -> if na /= a
                    then do
                        --print nas
                        return (AppState 
                            (edges newState) 
                            (nodes newState // [(v, Sink res na ref)]) 
                            (action newState)
                            (change newState))
                        --fmap (// [(v, Sink res na ref)]) ns
                    else return newState
        Nothing  -> return newState
    where
        addEdge e1 es = 
            if edgeInList e1 es 
            then filter (e1 /=) es 
            else (e1:es)
        edgeInList _ [] = False
        edgeInList (s1, (res1, _), e1) ((s2, (res2, _), e2):es)
            | s1==s2 && res1==res2 && e1==e2 = True
            | otherwise = edgeInList (s1, (res1, 0.0), e1) es
        removeEdges vx es = 
            foldr 
                (\(v1, e, v2) ns -> 
                    if v1==vx || v2==vx 
                    then ns 
                    else (v1,e,v2):ns) 
                [] es
        removeNode vx ns = ns // [(vx, EmptyNode)]

drawNode state nodeForm v (Machine rec a) = do
    let size = max (length $ inputs rec) (length $ outputs rec)
    setNextWindowSize (nodeForm !! size) ImGuiCond_None
    withCString ((name rec)++"###"++(show v)) \namePtr ->
        with (0) \boolPtr ->
            Raw.begin 
                namePtr 
                (Just boolPtr) 
                (Just $ ImGuiWindowFlags nodeWindowFlags)
    
    let tableO = TableOptions ImGuiTableFlags_None (ImVec2 (sinkWidth) (fromIntegral (size*round nodeExtraHeight))) 0
    
    beginTable tableO (T.pack $ show v) 2 
    
    Raw.tableNextRow ImGuiTableRowFlags_None (nodeExtraHeight-2)
    Raw.tableNextColumn
    text . T.pack $ name rec
    
    Raw.tableNextColumn
    remBut <- button "Remove"
    newState <- case remBut of
        True  -> do
            st <- state
            return (AppState 
                (removeEdges v (edges st)) 
                (removeNode v (nodes st)) 
                (action st)
                (change st))
        False -> state
    
    
    Raw.tableNextRow ImGuiTableRowFlags_None (nodeExtraHeight-2)
    Raw.tableNextColumn
    text . T.pack $ show a

    newActions <- mapM
            (\(i, o) -> resourceButtons i o)
            (take size $ zip (inputs rec++repeat ("",0)) (outputs rec++repeat ("",0)))
    endTable
    end
    
    case newAction newActions of
        (NonAction, NonAction)              -> return newState
        (EdgeConnectIn v1 res1, NonAction)  -> 
            case action newState of
                NonAction               -> 
                    return $ setAction (EdgeConnectIn v1 res1) newState
                EdgeConnectOut v2 res2  -> 
                    if res1 == res2
                    then return (AppState
                        (addEdge (v2, (res1, 0.0), v1) (edges newState))
                        (nodes newState)
                        NonAction
                        True)
                    else return $ setAction (EdgeConnectOut v2 res2) newState
                EdgeConnectIn v2 res2   -> 
                    if v2==v1 && res1 == res2
                    then return $ setAction NonAction newState
                    else return $ setAction (EdgeConnectIn v1 res1) newState
                --_                       -> return newState
        (NonAction, EdgeConnectOut v1 res1) ->
            case action newState of
                NonAction               -> 
                    return $ setAction (EdgeConnectOut v1 res1) newState
                EdgeConnectIn v2 res2  -> 
                    if res1 == res2
                    then return (AppState
                        (addEdge (v1, (res1, 0.0), v2) (edges newState))
                        (nodes newState)
                        NonAction
                        True)
                    else return $ setAction (EdgeConnectIn v2 res2) newState
                EdgeConnectOut v2 res2  -> 
                    if v2==v1 && res1 == res2
                    then return $ setAction NonAction newState
                    else return $ setAction (EdgeConnectOut v1 res1) newState
                --_                       -> return newState
        _                                   -> return newState
    where
        resourceButtons :: (Resource, Double) 
                        -> (Resource, Double) 
                        -> IO (Action, Action)
        resourceButtons i o = do
            Raw.tableNextRow ImGuiTableRowFlags_None nodeExtraHeight
            Raw.tableNextColumn
            inA <- if (snd i /= 0)
                   then do
                        button (T.pack ((take 4 $ fst i)++" "++(show $ snd i))) 
                            >>= \case
                                False -> return NonAction
                                True -> return (EdgeConnectIn v (fst i))
                   else do
                        return NonAction
                
            Raw.tableNextColumn
            outA <- if (snd o /= 0)
                    then do
                        button (T.pack ((take 4 $ fst o)++" "++(show $ snd o))) 
                            >>= \case
                                False -> return NonAction
                                True -> return (EdgeConnectOut v (fst o))
                    else do
                        return NonAction
            return (inA, outA)

        newAction :: [(Action, Action)] -> (Action, Action)
        newAction [] = (NonAction, NonAction)
        newAction as = foldr 
            (\(a1, a2) (ao1, ao2) -> (takeAction a1 ao1, takeAction a2 ao2)) 
            (NonAction, NonAction) 
            as
        takeAction :: Action -> Action -> Action
        takeAction NonAction a2 = a2
        takeAction a1 _ = a1
        addEdge e1 es = 
            if edgeInList e1 es 
            then filter (e1 /=) es 
            else (e1:es)
        edgeInList _ [] = False
        edgeInList (s1, (res1, _), e1) ((s2, (res2, _), e2):es)
            | s1==s2 && res1==res2 && e1==e2 = True
            | otherwise = edgeInList (s1, (res1, 0.0), e1) es
        removeEdges vx es = 
            foldr 
                (\(v1, e, v2) ns -> 
                    if v1==vx || v2==vx 
                    then ns 
                    else (v1,e,v2):ns) 
                [] es
        removeNode vx ns = ns // [(vx, EmptyNode)]

drawEdge :: Table Node -> Edge (Resource, Double) -> IO ()
drawEdge nodes (v1, (res, _), v2) = do
    begin (T.pack ("###"++(show v1)))
    pos <- getWindowPos
    let startPos = addToVec pos sinkWidth (connectionHeightOut (nodes ! v1) res)
    end
    begin (T.pack ("###"++(show v2)))
    pos <- getWindowPos
    let endPos = addToVec pos 0 (connectionHeightIn (nodes ! v2) res)
        middle1 = addToVec startPos edgeInitialStraight 0
        middle2 = addToVec endPos (-edgeInitialStraight) 0
    bg <- getBackgroundDrawList
    Foreign.with (startPos) \sp -> 
        Foreign.with (middle1) \m1 -> 
            Foreign.with (middle2) \m2 -> 
                Foreign.with (endPos) \ep -> 
                    DrawList.addBezierCubic
                        bg
                        sp m1 m2 ep
                        (Raw.imCol32 255 255 255 0xFF)
                        2.0
                        20
    end
    where
        resInd [] _ = 0
        resInd ((x, _):xs) r = if x==r then 0 else 1 + resInd xs r 
        addToVec (ImVec2 x y) xa ya = ImVec2 (x+xa) (y+ya)

connectionHeightIn :: Node -> Resource -> Float
connectionHeightIn (Sink _ _ _) _ = fromIntegral (div sinkHeight 2) 
connectionHeightIn (Machine rec _) r =
    realToFrac (sinkHeight)
    +realToFrac (nodeExtraHeight*(resInd (inputs rec) r))
    --realToFrac (nodeExtraHeight/2)
    where
        resInd [] _ = 0
        resInd ((x, _):xs) r = if x==r then 0 else 1 + resInd xs r 
connectionHeightOut :: Node -> Resource -> Float
connectionHeightOut (Sink _ _ _) _ = fromIntegral (div sinkHeight 2) 
connectionHeightOut (Machine rec _) r = 
    realToFrac (sinkHeight)
    +realToFrac (nodeExtraHeight*(resInd (outputs rec) r))
    --realToFrac (nodeExtraHeight/2)
    where
        resInd [] _ = 0
        resInd ((x, _):xs) r = if x==r then 0 else 1 + resInd xs r 

drawSinkMenu :: [Resource] -> IO (Maybe Node)
drawSinkMenu rs = do
    pressed <- mapM
        (\res -> drawSinkButton res)
        rs
    return $ firstValue pressed
    where
        firstValue [] = Nothing
        firstValue ((Just s):xs) = Just s
        firstValue (Nothing:xs) = firstValue xs
        drawSinkButton r = do
            button (T.pack r) >>= \case
                True -> do
                    ref <- newIORef "10"
                    return $ Just (Sink r 10.0 ref)
                False -> return Nothing

drawSourceMenu :: [Resource] -> IO (Maybe Node)
drawSourceMenu rs = do
    pressed <- mapM
        (\res -> drawSourceButton res)
        rs
    return $ firstValue pressed
    where
        firstValue [] = Nothing
        firstValue ((Just s):xs) = Just s
        firstValue (Nothing:xs) = firstValue xs
        drawSourceButton r = do
            button (T.pack r) >>= \case
                True -> do
                    return $ Just (Machine (Recipe r [] [(r, 1.0)]) 0.0)
                False -> return Nothing

drawNodeMenu :: [Recipe] -> IO (Maybe Node)
drawNodeMenu rc = do
    pressed <- mapM
        (\rec -> drawNodeButton rec)
        rc
    return $ firstValue pressed
    where
        firstValue [] = Nothing
        firstValue ((Just s):xs) = Just s
        firstValue (Nothing:xs) = firstValue xs
        drawNodeButton r = do
            button (T.pack $ name r) >>= \case
                True -> do
                    return $ Just (Machine r 0.0)
                False -> return Nothing

drawNodeTab :: String -> [Recipe] -> IORef Bool -> IO (Maybe Node)
drawNodeTab cat recs tabBoolRef = do
    nodeTab <- beginTabItem 
        (T.pack cat) 
        (tabBoolRef) 
        (ImGuiTabBarFlags tabBarFlags)
    newNode <- do
        if nodeTab
        then do
            newNode <- drawNodeMenu (recs)
            endTabItem
            return newNode
        else return Nothing
    return newNode
    

mainLoop :: AppConfig -> AppState -> IO ()
mainLoop config state = unlessQuit do
    
    -- Tell ImGui we're starting a new frame
    openGL3NewFrame
    sdl2NewFrame
    newFrame
   
    -- Keep tabs
    forM_ (tabBools config) (\ref -> writeIORef ref True)

    -- Draw Graph
    newState <- foldr 
            (\(v, n) ns -> drawNode ns (nodeForms config) v n) 
            (return state)
            (assocs $ nodes state)
    
    mapM_ (drawEdge (nodes state)) (edges state)

    -- Draw new edge
    case (action state) of
        EdgeConnectIn v1 res    -> do
            begin (T.pack ("###"++show v1))
            bg <- getBackgroundDrawList
            pos <- getWindowPos
            mousePos <- SDL.getAbsoluteMouseLocation
            let endPos = addToVec pos 0 (connectionHeightIn ((nodes state) ! v1) res)
                startPos = toImVec2 mousePos
                middle2 = addToVec endPos (-edgeInitialStraight) 0
                middle1 = addToVec startPos edgeInitialStraight 0
            Foreign.with (startPos) \sp -> 
                Foreign.with (middle1) \m1 -> 
                    Foreign.with (middle2) \m2 -> 
                        Foreign.with (endPos) \ep -> 
                            DrawList.addBezierCubic
                                bg
                                sp m1 m2 ep
                                (Raw.imCol32 255 255 255 0xFF)
                                2.0
                                20
            end
        EdgeConnectOut v1 res    -> do
            begin (T.pack ("###"++show v1))
            bg <- getBackgroundDrawList
            pos <- getWindowPos
            mousePos <- SDL.getAbsoluteMouseLocation
            let startPos = addToVec pos sinkWidth (connectionHeightOut ((nodes state) ! v1) res)
                endPos = toImVec2 mousePos
                middle2 = addToVec endPos (-edgeInitialStraight) 0
                middle1 = addToVec startPos edgeInitialStraight 0
            Foreign.with (startPos) \sp -> 
                Foreign.with (middle1) \m1 -> 
                    Foreign.with (middle2) \m2 -> 
                        Foreign.with (endPos) \ep -> 
                            DrawList.addBezierCubic
                                bg
                                sp m1 m2 ep
                                (Raw.imCol32 255 255 255 0xFF)
                                2.0
                                20
            end
        _                       -> return ()

    -- Show the ImGui demo window
    --showDemoWindow

    -- Menu window
    withCString ("Menu") \namePtr ->
        with (0) \boolPtr ->
            Raw.begin 
                namePtr 
                (Just boolPtr) 
                (Just $ ImGuiWindowFlags menuWindowFlags)
    
    update <- button "Update graph"
    
    text "Add node"
    
    beginTabBar "###Categories" $ ImGuiTabBarFlags tabBarFlags
   
    -- Sink tab
    sinks <- beginTabItem 
        "Sinks" 
        ((tabBools config) !! 0) 
        (ImGuiTabBarFlags tabBarFlags)
    newSink <- do
        if sinks
        then do
            newSink <- drawSinkMenu (resources config)
            endTabItem
            return newSink
        else return Nothing

    -- Source tab
    sources <- beginTabItem 
        "Sources" 
        ((tabBools config) !! 1) 
        (ImGuiTabBarFlags tabBarFlags)
    newSource <- do
        if sources
        then do
            newSource <- drawSourceMenu (resources config)
            endTabItem
            return newSource
        else return Nothing

    -- Recipe tabs
    newNodes <- mapM
        (\(bol, (cat, recs)) -> drawNodeTab cat recs bol)
        (zip (tail . tail $ tabBools config) (splitToCategories $ recipes config))

    endTabBar

    end
    
    newState <- case newSink of
        Nothing -> return newState
        Just s -> do
            putStrLn "Adding new Sink"
            return $ addNode newState s
    
    newState <- case newSource of
        Nothing -> return newState
        Just s -> do
            putStrLn "Adding new Source"
            return $ addNode newState s

    newState <- case firstValue newNodes of
        Nothing -> return newState
        Just s -> do
            putStrLn "Adding new Machine"
            return $ addNode newState s

    -- Render
    glClear GL_COLOR_BUFFER_BIT
    render
    openGL3RenderDrawData =<< getDrawData
    SDL.glSwapWindow $ window config

    -- Next frame
    if (update)
    then do 
        putStrLn "Updating graph..."
        mainLoop config $ updateState newState
    else 
        mainLoop config newState

    where
        firstValue [] = Nothing
        firstValue ((Just s):xs) = Just s
        firstValue (Nothing:xs) = firstValue xs
        toImVec2 (SDL.P (V2 x y)) = ImVec2 (fromIntegral x) (fromIntegral y)
        addToVec (ImVec2 x y) xa ya = ImVec2 (x+xa) (y+ya)
        resolveNodes ns = 
            snd $ resolveGraph ((graphFromEdges $ edges state), (resetNodes ns))
        resetNodes ns = fmap resetNode ns
        resetNode (Machine rec _) = Machine rec 0
        resetNode sink = sink

        updateState (AppState es ns act fl) = AppState es (resolveNodes ns) act fl
        addNode :: AppState -> Node -> AppState
        addNode (AppState es ns act fl) n = AppState es (addToArray ns n) act fl
        addToArray :: Table Node -> Node -> Table Node
        addToArray ns n = array 
            (fst $ bounds ns, 1 + (snd $ bounds ns)) 
            ((1 + (snd $ bounds ns), n):(assocs ns))
        
        splitToCategories :: [(String, Recipe)] -> [(String, [Recipe])]
        splitToCategories rs = 
            Map.assocs $ 
                foldr (\(cat, rec) -> Map.insertWith (++) cat [rec]) Map.empty rs

        -- Process the event loop
        unlessQuit action = do
            shouldQuit <- checkEvents
            if shouldQuit then pure () else action

        checkEvents = do
            pollEventWithImGui >>= \case
                Nothing ->
                    return False
                Just event ->
                    (isQuit event ||) <$> checkEvents

        isQuit event =
            SDL.eventPayload event == SDL.QuitEvent


