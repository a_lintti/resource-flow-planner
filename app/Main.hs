{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
module Main where

import Graph
import GUI
import Recipes

import Data.Array
import Data.IORef
import Control.Monad
import Control.Monad.State
import Control.Monad.Reader
import Foreign.C.Types (CInt(..))
import DearImGui
import DearImGui.SDL
import DearImGui.SDL.OpenGL
import DearImGui.OpenGL3
import Graphics.GL
import qualified SDL
import Linear (V4(..), V2(..))



main :: IO ()
main = do
    SDL.initializeAll

    window <- do
        let config = SDL.defaultWindow {
            SDL.windowOpenGL = Just SDL.defaultOpenGL,
            SDL.windowResizable = True,
            SDL.windowInitialSize = V2 1080 720}
        SDL.createWindow "My SDL Application" config
    
    glContext <- SDL.glCreateContext window
    imGuiContext <- createContext
    _ <- sdl2InitForOpenGL window glContext
    _ <- openGL3Init

    -- If recipe has more than 20 inputs or outputs this won't work.
    nodeForms <- mapM 
        (\i -> newIORef $ ImVec2 
            sinkWidth
            (fromIntegral sinkHeight + fromIntegral (i*round nodeExtraHeight))) 
        [0..20]
    
    let dataJSON = "recipe.json"
    recipes <- getRecipes dataJSON >>= \case
        Just l -> do
            putStrLn "Parsing recipes done."
            putStrLn ((take 50 (show l))++"...")
            return l
        Nothing -> do 
            putStrLn "Could not load .json"
            return []
    
    resources <- getResources dataJSON >>= \case
        Just l -> do
            putStrLn "Parsing resources done."
            putStrLn ((take 50 (show l))++"...")
            return l
        Nothing -> do 
            putStrLn "Could not load .json"
            return []

    gategories <- getGategories dataJSON >>= \case
        Just l -> do
            putStrLn "Parsing Categories done."
            putStrLn ((take 50 (show l))++"...")
            return l
        Nothing -> do 
            putStrLn "Could not load .json"
            return []
    
    tabBool <- mapM
        (\b -> newIORef b)
        (True:(take (1+length gategories) $ repeat True))

    let config = AppConfig window nodeForms tabBool recipes resources gategories
    --let state = AppState [(2, ("iron", 0.0), 1)] (array (1,2) [(1, Sink "iron" 20.0), (2, Sink "copper" 10.0)]) (NonAction) 

    yelRef <- newIORef "10"
    redRef <- newIORef "10"

    let state = AppState [] (array (1,1) [(1,EmptyNode)]) NonAction True

    liftIO $ mainLoop config state

    sdl2Shutdown
    openGL3Shutdown
    destroyContext imGuiContext
    SDL.glDeleteContext glContext
    SDL.destroyWindow window

