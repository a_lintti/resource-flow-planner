## Resource flow planner

Graphical resource flow planner. Intended use case is visualizing resource flows in the game Factorio.


### Major External Libraries
* SDL2 for window
* Dear ImGui for GUI
* Aeson for JSON parsing

![](example.png)

